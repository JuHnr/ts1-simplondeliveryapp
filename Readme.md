# SimplonDelivery App

## Présentation

SimplonDelivery App est une application conçue pour gérer la conversion de devises (EUR, CAD, JPY) et le calcul des frais de livraison et de douanes pour l'envoi de colis depuis la France vers le Canada ou le Japon.

## Prérequis

Installer Node.js et npm 

## Exécuter l'application

1. **Clonez le dépôt** 

2. **Récupérez les modules**

```
npm ci
```

3. **Lancez l'application**

```
npx tsc && node dist/index.js
```

## Remarques

Les conversions ne fonctionnent qu'entre l'euro (**EUR**), le dollar canadien (**CAD**) et le yen (**JPY**).

Les frais de livraison prennent en compte une taxe supplémentaire en cas de dimensions > 150cm.



