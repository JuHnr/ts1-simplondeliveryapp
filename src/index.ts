const readline = require('node:readline');


let rl = readline.createInterface(
    process.stdin, process.stdout);

console.log("\nBienvenue sur SimplonDelivery App.\nIci vous pourrez : ");
console.log("1. Convertir un montant d'une devise à une autre");
console.log("2. Calculer les frais de livraison");
console.log("3. Calculer les frais de douanes");

//TODO entrée pas sensible à la casse, vérifier que c'est rien d'autre que EUR, CAD, JPY (3 lettres max) ou CA, JP, FR (2 lettres max)
//TODO valeur numérique > 0 (montant, dimension, vérifier que c'est bien un nombre)

rl.question('\nQue souhaitez-vous faire (1, 2 ou 3) ? \n', (choice: string) => {
    //vérifie l'input de l'utilisateur
    if (choice === "1" || choice === "2" || choice === "3") {

        //Convertisseur de devises
        if (choice === "1") {
            console.log(`\nVous souhaitez convertir un montant d'une devise à une autre.`);

            rl.question('Quel est le montant à convertir : ', (amountInput: string) => {
                //vérifie qu'une valeur a bien été rentrée
                if (amountInput === "") {
                    console.log("Le montant n'a pas été renseigné. Veuillez recommencer.");
                    rl.close();
                    return;

                    //si elle a bien été rentrée
                } else {
                    // Converti l'input string en nombre
                    const amount = parseFloat(amountInput);
                    //vérifie que la valeur est un nombre positif
                    if (amount < 0 || isNaN(amount)) {
                        console.log("Montant invalide. Veuillez recommencer.");
                        rl.close();
                        return;

                        //si c'est bien un nombre positif
                    } else {
                        rl.question('Devise de départ (EUR / CAD / JPY) : ', (sourceCurrency: string) => {
                            sourceCurrency = sourceCurrency.toUpperCase().trim();
                            //valide la devise de départ
                            if (sourceCurrency !== "EUR" && sourceCurrency !== "CAD" && sourceCurrency !== "JPY") {
                                console.log("Seules les devises EUR, CAD et JPY sont acceptées. Veuillez recommencer.")
                                rl.close();
                                return;
                            }

                            rl.question('Devise cible (EUR / CAD / JPY) : ', (targetCurrency: string) => {
                                targetCurrency = targetCurrency.toUpperCase().trim();
                                //valide la devise cible
                                if (targetCurrency !== "EUR" && targetCurrency !== "CAD" && targetCurrency !== "JPY") {
                                    console.log("Seules les devises EUR, CAD et JPY sont acceptées. Veuillez recommencer.")
                                    rl.close();
                                    return;
                                }

                                //si les deux devises sont validées, exécute la fonction de conversion
                                let result = convert(amount, sourceCurrency, targetCurrency);
                                console.log(`${amount} ${sourceCurrency} est équivalent à ${result} ${targetCurrency}.`)
                                rl.close();
                            })
                        });
                    }
                }
            });
        }

        //Calculateur de frais de livraison
        if (choice === "2") {
            console.log(`Vous souhaitez calculer les frais de livraison.`);

            rl.question('Quel est le pays de destination (FR, CA, JP) ? : ', (destination: string) => {
                destination = destination.toUpperCase().trim();
                //vérifie qu'une valeur a bien été rentrée
                if (destination === "") {
                    console.log("Le pays de destination n'a pas été renseigné. Veuillez recommencer.");
                    rl.close();
                    return;

                    //si elle a bien été rentrée, vérifie que la valeur est correcte
                } else if (destination !== "FR" && destination !== "CA" && destination !== "JP") {
                    console.log("Veuillez indiquer FR, CA ou JP. Merci de recommencer.")
                    rl.close();
                    return;

                    //si elle est correcte
                } else {
                    rl.question('Quel est le poids de votre colis (en kg) : ', (weightInput: string) => {

                        //si elle a bien été rentrée
                        console.log('Indiquez les dimensions du colis')
                        rl.question(' - longueur (en cm) : ', (lengthInput: string) => {
                            rl.question(' - largeur (en cm) : ', (widthInput: string) => {
                                rl.question(' - hauteur (en cm) : ', (heightInput: string) => {
                                    //vérifie qu'une valeur a bien été rentrée pour le poids et les dimensions
                                    if (weightInput === "" || lengthInput === "" || widthInput === "" || heightInput === "") {
                                        console.log("Le poids ou les dimensions n'ont pas été correctement renseigné. Veuillez recommencer.");
                                        rl.close();
                                        return;
                                    }

                                    // Converti tous les inputs string en nombre
                                    const weight = parseFloat(weightInput);
                                    const length = parseFloat(lengthInput);
                                    const width = parseFloat(widthInput);
                                    const height = parseFloat(heightInput);

                                    //vérifie que les valeurs sont des nombres
                                    if (isNaN(weight) || isNaN(length) || isNaN(width) || isNaN(height)) {
                                        console.log("Poids ou dimensions invalide(s). Veuillez recommencer.");
                                        rl.close();
                                        return;

                                        //vérifie que les valeurs sont des nombres positifs
                                    } else if (weight < 0 || length < 0 || width < 0 || height < 0) {
                                        console.log("Le poids et les dimensions doivent être positifs. Veuillez recommencer.");
                                        rl.close();
                                        return;
                                    }

                                    //si c'est bien un nombre positif, exécute la fonction de calcul des frais de livraison
                                    let result = calculateDeliveryFees(destination, weight, length, width, height);
                                    console.log(`Les frais de livraison estimés sont de : ${result}.`)
                                    rl.close();
                                }
                                )
                            })
                        });
                    });
                }
            })

        }

        //Calculateur de frais de douanes
        if (choice === "3") {
            console.log(`Vous souhaitez calculer les frais de douanes pour un envoi depuis la France.`);
            rl.question('Quel est le pays de destination (CA, JP) ? : ', (destination: string) => {
                destination = destination.toUpperCase().trim();
                //vérifie qu'une valeur a bien été rentrée
                if (destination === "") {
                    console.log("Le pays de destination n'a pas été renseigné. Veuillez recommencer.");
                    rl.close();
                    return;

                    //si elle a bien été rentrée, vérifie que la valeur est correcte
                } else if (destination !== "CA" && destination !== "JP") {
                    console.log("Veuillez indiquer CA (Canada) ou JP (Japon). Merci de recommencer.")
                    rl.close();
                    return;

                    //si elle est correcte
                } else {
                    rl.question('Valeur du colis (dans la devise du pays de destination): ', (valueInput: string) => {
                        //vérifie qu'une valeur a bien été rentrée
                        if (valueInput === "") {
                            console.log("La valeur du colis n'a pas été renseigné. Veuillez recommencer.");
                            rl.close();
                            return;
                        }

                        // Converti les inputs string en nombre
                        const value = parseFloat(valueInput);

                        //vérifie que la valeur est un nombre positif
                        if (isNaN(value) || value < 0) {
                            console.log("Valeur invalide. Veuillez recommencer.");
                            rl.close();
                            return;
                        }

                        //exécute la fonction de calcul des frais de douanes
                        let result = calculateCustomsFees(destination, value);
                        console.log(`Les frais de douanes estimés sont de : ${result}.`)
                        rl.close();

                    });
                }

            });
        }

    } else {
        console.log("Veuillez indiquer 1, 2 ou 3.");
        rl.close();
    };
});


/** Convertisseur
*
*@param amount montant à convertir
*@param sourceCurrency devise source
*@param targetCurrency devise cible
*@returns le montant en devise source converti en devise cible
*
*/

function convert(amount: number, sourceCurrency: string, targetCurrency: string): number {

    if (sourceCurrency === "EUR") {
        switch (targetCurrency) {
            case "CAD": {
                return amount * 1.5;
            }
            case "JPY": {
                return amount * 130;
            }
            default:
                return amount;
        }

    } else if (sourceCurrency === "CAD") {
        switch (targetCurrency) {
            case "EUR": {
                return amount * 0.67;
            }
            case "JPY": {
                return amount * 87;
            }
            default:
                return amount;
        }

    } else if (sourceCurrency === "JPY") {
        switch (targetCurrency) {
            case "EUR": {
                return amount * 0.0077;
            }
            case "CAD": {
                return amount * 0.0115;
            }
            default:
                return amount;
        }
    }

    return amount;
}

/** Calculateur de frais de livraison
*
*@param destination : destination du colis (france, canada ou japon)
*@param weight : poids du colis
*@param length, width, height :  dimensions du colis
*@returns les frais de livraisons dans la devise du pays de destination (en tenant compte du poids et des dimensions => voir weightFees et outsizeFees)
*
*/

function calculateDeliveryFees(destination: string, weight: number, length: number, width: number, height: number): string {
    let currency: string;
    let extraFees: number;
    let totalFees: number;
    let result: string = "";
    switch (destination) {
        case "FR": {
            currency = "EUR";
            totalFees = weightFees(weight, 10, 20, 30) + outsizeFees(length, width, height, 5);
            return result = `${totalFees} ${currency}`;
        }
        case "CA": {
            currency = "CAD";
            extraFees = outsizeFees(length, width, height, 7.5);
            totalFees = weightFees(weight, 15, 30, 45) + extraFees;
            return result = `${totalFees} ${currency}`;
        }
        case "JP": {
            currency = "JPY";
            extraFees = outsizeFees(length, width, height, 500);
            totalFees = weightFees(weight, 1000, 2000, 3000) + extraFees;
            return result = `${totalFees} ${currency}`;
        }
    }
    return result;
}

/**
*
* @param weight: poids du colis
* @param smallFees: frais du pays si colis < 1 kg
* @param mediumFees: frais du pays si colis < 3 kg
* @param heavyFees: frais du pays si colis >3 kg
* @returns les frais de livraisons selon le poids du colis et le pays
*
*/

function weightFees(weight: number, smallFees: number, mediumFees: number, heavyFees: number): number {
    if (weight <= 1) {
        return smallFees;
    } else if (weight <= 3) {
        return mediumFees;
    } else {
        return heavyFees;
    }
}

/**
*
* @param length, width, height: dimension du colis
* @param extraFees : frais supplémentaire du pays si la somme des dimensions > 150cm
* @returns les frais de livraisons supplémentaires
*
*/
function outsizeFees(length: number, width: number, height: number, extraFees: number): number {
    return length + width + height > 150 ? extraFees : 0;
}

//Calculateur de frais de douanes
/**
*
* @param destination : destination du colis
* @param value : valeur du colis dans la devise de destination
* @returns les frais de douanes appliqués avec la devise de destination
*
*/
function calculateCustomsFees(destination: string, value: number) {
    if (destination === "CA" && value > 20) {
        let currency: string = "CAD";
        let fees = value * 0.15;
        return `${fees} ${currency}`
    } else if (destination === "JP" && value > 5000) {
        let currency: string = "JPY";
        let fees = value * 0.10;
        return `${fees} ${currency}`
    } else {
        return "Aucun frais de douanes"
    }
}


